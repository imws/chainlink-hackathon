import json
import base64
import requests
from .futures import CompletableFuture
from .apicontext import ApiContext
from .keys import KeyExchange, readKey
from .session import Session

class ClientHttp:
    def __init__(self, config):
        self.config = config
        self.version = "v1"

        self.keyExchange = KeyExchange()

    def init(self):
        cfg = self.config["http"]

        self.apiUrl = ("https" if cfg["useHttps"] else "http") + "://" + cfg["host"] + ":" + str(cfg["port"])

        serverPublicKey = self.publicKey().get()["data"]
        clientPublicKey = readKey(self.config.get("publicKey"), self.config.get("publicKeyFile"))
        clientPrivateKey = readKey(self.config.get("privateKey"), self.config.get("privateKeyFile"))

        self.apiContext = ApiContext(
            bytes.fromhex(self.config["seed"]),
            serverPublicKey,
            clientPublicKey,
            clientPrivateKey
        )
        self.secretKey = self.keyExchange.sharedSecret(self.apiContext.clientPrivateKey, self.apiContext.serverPublicKey)

    def version(self):
        reply = requests.get(self.apiUrl + "/version")
        return reply.content.decode("utf-8")

    def get(self, call):
        url = self.apiUrl + "/" + self.version + "/" + call
        #print(url)
        reply = requests.get(url)
        return reply.content.decode("utf-8")

    def post(self, call, data, headers):
        url = self.apiUrl + "/" + self.version + "/" + call
        reply = requests.post(url, json=data, headers=headers)
        return reply.content.decode("utf-8")

    def ping(self):
        return CompletableFuture(self.get("ping"))

    def publicKey(self):
        return CompletableFuture(json.loads(self.get("public_key")))

    def signString(self, toSign):
        signed = self.keyExchange.encrypt(self.secretKey, toSign, self.apiContext.seed)
        return signed.hex()

    def login(self, organization, account, scopes):
        toSign = organization + "\n" + account + "\n" + scopes
        signature = self.signString(toSign)

        reply = self.post("login", {
            "organization": organization,
            "account": account,
            "scopes": scopes,
            "signature": signature
        }, None)

        data = json.loads(json.loads(reply)["data"])
        secret = bytes.fromhex(data["secret"])
        decryptedSecret = self.keyExchange.decrypt(self.secretKey, secret, self.apiContext.seed)

        del data["secret"]
        return CompletableFuture(Session(data, decryptedSecret))

    def authPost(self, session, call, data):
        data["organization"] = session.organization
        data["account"] = session.account

        body = json.dumps(data)
        nonce = str(session.getNonce())
        signature = self.keyExchange.signHTTP(
            session.secret,
            "/" + self.version + "/" + call,
            session.apiKey,
            nonce,
            body
        )
        headers = {
            "x-api-key": session.apiKey,
            "x-nonce": nonce,
            "x-sig": signature
        }

        return CompletableFuture(self.post(call, data, headers))

    def logout(self, session):
        return self.authPost(session, "logout", {})

    def status(self, session):
        return self.authPost(session, "status", {})

    def createTable(self, session, scope, table, createOptions):
        return self.authPost(session, "create", {
            "scope": scope,
            "table": table,
            "options": createOptions.toJson()
        })

    def write(self, session, scope, records, writeOptions):
        return self.authPost(session, "write", {
            "scope": scope,
            "table": records.table,
            "enc": "json",
            "records": records.toJson(),
            "options": writeOptions.toJson()
        })

    def read(self, session, scope, table, filter, readOptions):
        data = {
            "scope": scope,
            "table": table,
            "options": readOptions.toJson()
        }

        if filter is not None:
            data["filter"] = filter.toJson()
        return self.authPost(session, "read", data)

    def getNodes(self, session):
        data = {}
        return self.authPost(session, "get_nodes", data)

    def getScopes(self, session):
        data = {}
        return self.authPost(session, "get_scopes", data)

    def getTables(self, session, scope):
        data = {
            "scope": scope
        }
        return self.authPost(session, "get_tables", data)

    def getNodeConfig(self, session, nodePublicKey):
        data = {
            "nodePublicKey": nodePublicKey
        }
        return self.authPost(session, "get_node_config", data)

    def getAccountNotifications(self, session):
        data = {}
        return self.authPost(session, "account_notifications", data)
