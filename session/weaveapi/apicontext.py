import base58
import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization

class ApiContext:
    def __init__(self, seed, serverPublicKey, clientPublicKey, clientPrivateKey):
        self.seed = seed

        self.serverPublicKey = self.deserializePublic(serverPublicKey)
        self.clientPublicKey = self.deserializePublic(clientPublicKey)
        self.clientPrivateKey = self.deserializePrivate(clientPrivateKey, None)

    def deserializePublic(self, key):
        try:
            if key.startswith("weave"):
                key = key[5:]
            curve = ec.SECP256K1()
            return ec.EllipticCurvePublicKey.from_encoded_point(curve, base58.b58decode(key))
        except:
            try:
                return serialization.load_der_public_key(bytes.fromhex(key), default_backend())
            except:
                return serialization.load_der_public_key(base64.b64decode(key), default_backend())

    def deserializePrivate(self, key, password):
        try:
            curve = ec.SECP256K1()
            d = int(base58.b58decode(key).hex(), base=16)
            return ec.derive_private_key(d, curve)
        except:
            try:
                return serialization.load_der_private_key(bytes.fromhex(key), password, default_backend())
            except:
                return serialization.load_der_private_key(base64.b64decode(key), password, default_backend())