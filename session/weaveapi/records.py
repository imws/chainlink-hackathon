import json

class Records:
    def __init__(self, table, records):
        self.table = table
        self.records = records

    def get(self, index):
        return self.records[index]

    def toJson(self):
        return json.dumps({
            "table": self.table,
            "items": [ r.toJson() for r in self.records ]
        })

class Record:
    def __init__(self, id, data, metadata):
        self.id = id
        self.data = data
        self.metadata = metadata

    def toJson(self):
        return [
            self.id,
            self.data,
            self.metadata
        ]