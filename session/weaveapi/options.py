import json

class CreateOptions:
    def __init__(self, failIfExists):
        self.failIfExists = failIfExists

    def toJson(self):
        return json.dumps({
            "failIfExists": self.failIfExists
        })

CREATE_DEFAULT = CreateOptions(True)
CREATE_FAILSAFE = CreateOptions(False)


class ReadOptions:
    def __init__(self, verifyHash, readTimeoutSec):
        self.verifyHash = verifyHash
        self.readTimeoutSec = readTimeoutSec

    def toJson(self):
        return json.dumps({
            "verifyHash": self.verifyHash,
            "readTimeoutSec": self.readTimeoutSec
        })

DEFAULT_READ_TIMEOUT_SEC = 300

READ_DEFAULT = ReadOptions(True, DEFAULT_READ_TIMEOUT_SEC)
READ_DEFAULT_NO_CHAIN = ReadOptions(False, DEFAULT_READ_TIMEOUT_SEC)


class SubscribeOptions:
    def __init__(self, verifyHash, initialSnapshot, readTimeoutSec, externalUpdates,batchingOptions):
        self.verifyHash = verifyHash
        self.initialSnapshot = initialSnapshot
        self.readTimeoutSec = readTimeoutSec
        self.externalUpdates = externalUpdates
        self.batchingOptions = batchingOptions

    def toJson(self):
        return json.dumps({
            "verifyHash": self.verifyHash,
            "initialSnapshot": self.initialSnapshot,
            "readTimeoutSec": self.readTimeoutSec,
            "externalUpdates": self.externalUpdates,
            "batchingOptions": self.batchingOptions,
        })

SUBSCRIBE_DEFAULT = SubscribeOptions(True, True, DEFAULT_READ_TIMEOUT_SEC, False, None)

class WriteOptions:
    def __init__(self, guaranteed, minAcks, inMemoryAcks, minHashAcks, writeTimeoutSec, allowDistribute, signOnChain, syncSigning):
        self.guaranteed = guaranteed
        self.minAcks = minAcks
        self.inMemoryAcks = inMemoryAcks
        self.minHashAcks = minHashAcks
        self.writeTimeoutSec = writeTimeoutSec
        self.allowDistribute = allowDistribute
        self.signOnChain = signOnChain
        self.syncSigning = syncSigning

    def toJson(self):
        return json.dumps({
            "guaranteed": self.guaranteed,
            "minAcks": self.minAcks,
            "inMemoryAcks": self.inMemoryAcks,
            "minHashAcks": self.minHashAcks,
            "writeTimeoutSec": self.writeTimeoutSec,
            "allowDistribute": self.allowDistribute,
            "signOnChain": self.signOnChain,
            "syncSigning": self.syncSigning
        })

DEFAULT_GUARANTEED_DELIVERY = True
DEFAULT_MIN_ACKS = 1
DEFAULT_MEMORY_ACKS = False
DEFAULT_HASH_ACKS = 1
DEFAULT_WRITE_TIMEOUT_SEC = 300

WRITE_DEFAULT = WriteOptions(
        DEFAULT_GUARANTEED_DELIVERY,
        DEFAULT_MIN_ACKS,
        DEFAULT_MEMORY_ACKS,
        DEFAULT_HASH_ACKS,
        DEFAULT_WRITE_TIMEOUT_SEC,
        True,
        True,
        False
)

WRITE_DEFAULT_ASYNC = WriteOptions(
        False,
        DEFAULT_MIN_ACKS,
        True,
        0,
        DEFAULT_WRITE_TIMEOUT_SEC,
        True,
        True,
        False
)
WRITE_DEFAULT_NO_CHAIN = WriteOptions(
        DEFAULT_GUARANTEED_DELIVERY,
        DEFAULT_MIN_ACKS,
        DEFAULT_MEMORY_ACKS,
        0,
        DEFAULT_WRITE_TIMEOUT_SEC,
        True,
        False,
        False
)
