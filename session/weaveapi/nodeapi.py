from .clientws import ClientWs
from .clienthttp import ClientHttp
from .keys import readKey

class NodeApi:
    def __init__(self, config):
        self.config = config

    def init(self):
        cfg = self.config

        self.publicKey = readKey(cfg.get("publicKey"), cfg.get("publicKeyFile"))

        if cfg.get("websocket") is not None:
            self.client = ClientWs(cfg)
        elif cfg.get("http") is not None:
            self.client = ClientHttp(cfg)

        if self.client is not None:
            self.client.init()

    def getClientPublicKey(self):
        return self.publicKey

    def version(self):
        return self.client.version()

    def ping(self):
        return self.client.ping()

    def publicKey(self):
        return self.client.publicKey()

    def login(self, organization, account, scopes):
        return self.client.login(organization, account, scopes)

    def logout(self, session):
        return self.client.logout(session)

    def status(self, session):
        return self.client.status(session)

    def createTable(self, session, scope, table, createOptions):
        return self.client.createTable(session, scope, table, createOptions)

    def write(self, session, scope, records, writeOptions):
        return self.client.write(session, scope, records, writeOptions)

    def read(self, session, scope, table, filter, readOptions):
        return self.client.read(session, scope, table, filter, readOptions)

    def deployOracle(self, session, oracleType, targetBlockchain, source, deployOptions):
        return self.client.deployOracle(session, oracleType, targetBlockchain, source, deployOptions)
