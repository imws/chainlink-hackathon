import json
import time
import traceback
import uuid
import websocket
import threading
from .futures import CompletableFuture
from .keys import KeyExchange, readKey
from .apicontext import ApiContext
from .session import Session

class ClientWs:
    def __init__(self, config):
        self.config = config
        self.websocket = None

        self.keyExchange = KeyExchange()

    def init(self):
        cfg = self.config["websocket"]

        self.pendingRequests = {}

        self.apiUrl = ("wss" if cfg["useWss"] else "ws") + "://" + cfg["host"] + ":" + str(cfg["port"])

        self.thread = threading.Thread(target=self.wsloop)
        self.thread.start()

        while self.websocket is None:
            print("Waiting connection...")
            time.sleep(1)

        serverPublicKey = self.publicKey().get()["data"] 
        clientPublicKey = readKey(self.config.get("publicKey"), self.config.get("publicKeyFile"))
        clientPrivateKey = readKey(self.config.get("privateKey"), self.config.get("privateKeyFile"))

        self.apiContext = ApiContext(
            bytes.fromhex(self.config["seed"]),
            serverPublicKey,
            clientPublicKey,
            clientPrivateKey
        )
        self.secretKey = self.keyExchange.sharedSecret(self.apiContext.clientPrivateKey, self.apiContext.serverPublicKey)


    def on_message(self, ws, message):
        try:
            #print("Received: " + message)

            data = json.loads(message)
            id = data.get("id")
            req = None if id is None else self.pendingRequests.get(id)
            if req is not None:
                reply = data.get("reply")
                if reply is not None and reply.get("target") is not None \
                        and reply["target"].get("operationType") is not None \
                        and reply.get("data") is not None \
                        and reply["target"].get("operationType").lower() == "login":
                    sdata = json.loads(reply["data"])
                    secret = bytes.fromhex(sdata["secret"])
                    decryptedSecret = self.keyExchange.decrypt(self.secretKey, secret, self.apiContext.seed)
                    del sdata["secret"]
                    req.done(Session(sdata, decryptedSecret))
                else:
                    req.done(reply)
        except Exception as e:
            print("ERROR: Failed parsing message")
            print(traceback.format_exc())

    def on_error(self, ws, error):
        print(error)

    def on_close(self, ws, close_status_code, close_msg):
        print("websocket closed")

    def on_open(self, ws):
        self.websocket = ws

    def wsloop(self):
        print("Listening for events...")
        #websocket.enableTrace(True)
        ws = websocket.WebSocketApp(
            self.apiUrl,
            on_open=self.on_open,
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close
        )
        ws.run_forever()

    def call(self, data):
        id = str(uuid.uuid4()).replace("-", "")
        data["id"] = id

        future = CompletableFuture(None)
        self.pendingRequests[id] = future

        msg = json.dumps(data)
        #print("Sending: " + msg)
        self.websocket.send(msg)
        return future

    def version(self):
        return self.call({ "type": "version" })

    def ping(self):
        return self.call({ "type": "ping" })

    def publicKey(self):
        return self.call({ "type": "public_key" })

    def signString(self, toSign):
        signed = self.keyExchange.encrypt(self.secretKey, toSign, self.apiContext.seed)
        return signed.hex()

    def login(self, organization, account, scopes):
        toSign = organization + "\n" + account + "\n" + scopes
        signature = self.signString(toSign)

        return self.call({
            "type": "login",
            "organization": organization,
            "account": account,
            "scopes": scopes,
            "signature": signature
        })

    def authPost(self, session, data):
        data["x-api-key"] = session.apiKey
        data["x-nonce"] = session.getNonce()

        signature = self.keyExchange.signWS(session.secret, data)
        data["x-sig"] = signature

        return self.call(data)

    def logout(self, session):
        return self.authPost(session, {
            "type": "logout",
            "organization": session.organization,
            "account": session.account
        })

    def status(self, session):
        return self.authPost(session, {
            "type": "status",
            "organization": session.organization,
            "account": session.account
        })

    def createTable(self, session, scope, table, createOptions):
        return self.authPost(session, {
            "type": "create",
            "organization": session.organization,
            "account": session.account,
            "scope": scope,
            "table": table,
            "options": createOptions.toJson()
        })

    def write(self, session, scope, records, writeOptions):
        return self.authPost(session, {
            "type": "write",
            "organization": session.organization,
            "account": session.account,
            "scope": scope,
            "table": records.table,
            "enc": "json",
            "records": records.toJson(),
            "options": writeOptions.toJson()
        })

    def read(self, session, scope, table, filter, readOptions):
        data = {
            "type": "read",
            "organization": session.organization,
            "account": session.account,
            "scope": scope,
            "table": table,
            "options": readOptions.toJson()
        }

        if filter is not None:
            data["filter"] = filter.toJson()
        return self.authPost(session, data)

    def subscribe(self, session, scope, table, filter, subscribeOptions):
        data = {
            "type": "read",
            "organization": session.organization,
            "account": session.account,
            "scope": scope,
            "table": table,
            "options": subscribeOptions.toJson()
        }

        if filter is not None:
            data["filter"] = filter.toJson()
        return self.authPost(session, data)

    def getNodes(self, session):
        data = {
            "type": "get_nodes",
        }
        return self.authPost(session, data)

    def getScopes(self, session):
        data = {
            "type": "get_scopes",
        }
        return self.authPost(session, data)

    def getTables(self, session, scope):
        data = {
            "type": "get_node_config",
            "scope": scope
        }
        return self.authPost(session, data)

    def getNodeConfig(self, session, nodePublicKey):
        data = {
            "type": "get_node_config",
            "nodePublicKey": nodePublicKey
        }
        return self.authPost(session, data)

    def getAccountNotifications(self, session):
        data = {
            "type": "get_account_notifications",
        }
        return self.authPost(session, data)

    def deployOracle(self, session, oracleType, targetBlockchain, source, deployOptions):
        data = {
            "type": "deploy_oracle",
            "organization": session.organization,
            "account": session.account,
            "oracleType": oracleType,
            "targetBlockchain": targetBlockchain,
            "source": source,
            "options": None if deployOptions is None else json.dumps(deployOptions)
        }

        return self.authPost(session, data)