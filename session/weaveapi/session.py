import base64

class Session:
    def __init__(self, data, decryptedSecret):
        self.organization = data["organization"]
        self.account = data["account"]
        self.scopes = data["scopes"]
        self.apiKey = data["apiKey"]

        self.secret = base64.b64decode(decryptedSecret)
        self.secretExpireUTC = data["secretExpireUTC"]
        self.nonce = 0.0

    def getNonce(self):
        self.nonce += 1
        return self.nonce
