import json

class Filter:
    def __init__(self, op, order, limit):
        self.op = op
        self.order = order
        self.limit = limit

    def toJson(self):
        return json.dumps({
            "op": self.op,
            "order": self.order,
            "limit": self.limit
        })
