cd /app

cat contracts/WeaveConsumer.sol.template | gawk '{gsub("{PROVIDER_URL}", ENVIRON["PROVIDER_URL"], $0); gsub("{PROVIDER_PATH}", ENVIRON["PROVIDER_PATH"], $0); print $0; }' > contracts/WeaveConsumer.sol

yarn hardhat deploy --network ${DEPLOY_NETWORK}

cat artifacts/contracts/WeaveConsumer.sol/WeaveConsumer.json
