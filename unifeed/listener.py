import json
import time
import csv
import asyncio
import threading
import traceback

import numpy as np

from web3 import Web3
from web3._utils.events import get_event_data

from weaveapi import weaveapi
from weaveapi.records import *
from weaveapi.options import *

WEB3_PROVIDER = 'https://eth.hyperplane.dev/'

POOLS_FILE = 'pools.csv'

INIT_BLOCKS = 10000
WINDOW_LEN = 100
POOL_THROTTLE_SEC = 5
PERSIST_DATA = False
USE_TIMESTAMPS = False

WEAVE_CONFIG = "weave.config"

with open(WEAVE_CONFIG) as f:
    config = json.loads(f.read())
    print(config)

organization = config["organization"]
account = config["account"]
scope = config["scope"]
table = config["table"]

async def evt_listener(web3, nodeApi, session, pair, event_filter, event_list):
    lastBlock = None
    lastTimestamp = None

    while True:
        for evt in event_filter.get_new_entries():
            if USE_TIMESTAMPS:
                if evt.blockNumber == lastBlock:
                    timestamp = lastTimestamp
                else:
                    lastBlock = evt.blockNumber
                    block = web3.eth.getBlock(evt.blockNumber)
                    timestamp = block.timestamp
                    lastTimestamp = timestamp

                data = json.loads(web3.toJSON(evt))
                data['timestamp'] = timestamp
            else:
                data = evt

            event_list.append(data)
            compute(nodeApi, session, pair, event_list) #maybe throttle computations

        await asyncio.sleep(POOL_THROTTLE_SEC)

def wait_events(web3, nodeApi, session, pair, contract, event_list):
    event = contract.events.Swap
    event_filter = event.createFilter(fromBlock='latest')

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        loop.run_until_complete(asyncio.gather(evt_listener(web3, nodeApi, session, pair, event_filter, event_list)))
    finally:
        loop.close()

def hurst_ec(ts, lags=range(2, 20)):
    vt = []
    tau = []
    for lag in lags:
        tau.append(lag)
        pp = np.subtract(ts[lag:], ts[:-lag])
        vt.append(np.var(pp))

    m = np.polyfit(np.log10(tau), np.log10(vt), 1)
    return m[0] / 2

def compute(nodeApi, session, pair, event_list):
    try:
        ts = []
        flow = 0
        for e in event_list:
            amount0 = e['args']['amount0']
            flow += amount0
            ts.append(flow)

        if PERSIST_DATA:
            with open("data_" + pair.replace('/', '_') + '.json', 'w') as f:
                f.write(json.dumps(event_list))

        wnd = ts[-WINDOW_LEN:] if len(ts) > WINDOW_LEN else ts
        h = hurst_ec(wnd)

        now = round(time.time() * 1000)
        rec = {}
        rec[pair] = { "hurst": h, "ts": now }
        records = Records(table, [ Record(now, json.dumps(rec), None) ])
        nodeApi.write(session, scope, records, WRITE_DEFAULT)

        print(pair, len(event_list), h)
    except:
        #print(traceback.format_exc())
        pass

def get_history(web3, contract, event_list):
    latest = web3.eth.get_block('latest')
    blocknum = latest.number - INIT_BLOCKS

    hist = web3.eth.get_logs({"fromBlock": blocknum, "address": [ contract.address ]})

    event = contract.events.Swap
    codec = event.web3.codec
    event_abi = event._get_event_abi()

    lastBlock = None
    lastTimestamp = None

    for evt in hist:
        try:
            event_data = get_event_data(codec, event_abi, evt)

            if USE_TIMESTAMPS:
                if evt.blockNumber == lastBlock:
                    timestamp = lastTimestamp
                else:
                    lastBlock = evt.blockNumber
                    block = web3.eth.getBlock(evt.blockNumber)
                    timestamp = block.timestamp
                    lastTimestamp = timestamp

                data = json.loads(web3.toJSON(event_data))
                data['timestamp'] = timestamp
            else:
                data = event_data

            event_list.append(data)
        except:
            pass

def main():
    chainConfig = config["chainClientConfig"]
    nodeApi = weaveapi.create(chainConfig)
    nodeApi.init()

    print(nodeApi.ping().get())

    session = nodeApi.login(organization, account, scope).get()
    nodeApi.createTable(session, scope, table, CREATE_FAILSAFE).get()

    pk = 'privatekey'
    deployOptions = { 
        'sync': 0,
        'params': {
            'CHAINLINK_FEE': '100000000000000000',
            'PRIVATE_KEY': pk
        }
    }
    source = [ scope, table, "data" , "USDC/USDT", "hurst" ]
    nodeApi.deployOracle(session, "chainlink", "rinkeby", ",".join(source), deployOptions)

    web3 = Web3(Web3.HTTPProvider(WEB3_PROVIDER))

    pools = {}
    with open(POOLS_FILE) as f:
        for r in csv.reader(f, delimiter=','):
            pools[r[0]] = r[1]

    for pair, pool_addr in pools.items():
        print("Initializing", pair)

        with open("poolabi.json") as f:
            pool_abi = json.loads(f.read())

        contract = web3.eth.contract(address=pool_addr, abi=pool_abi)

        event_list = []
        get_history(web3, contract, event_list)
        compute(nodeApi, session, pair, event_list)

        thr = threading.Thread(target=wait_events, args=(web3, nodeApi, session, pair, contract, event_list), daemon=True)
        thr.start()

    while True:
        time.sleep(1)

if __name__ == "__main__":
    main()
