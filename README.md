<br/>
<br/>

# Chainlink Hackathon May 2022

### Publishing complex chain analytics back on-chain

This project is a sample showcasing off-chain computations of complex analytics (that are impossible or too costly to be made on blockchain's virtual machines) and publishing them back on-chain for Web3 consumers. We build the flow time series for several [Uniswap](https://uniswap.org/) liquidity pools and compute the [hurst exponent](https://en.wikipedia.org/wiki/Hurst_exponent) of these time series for a recent window of time. These measures are then published via Chainlink with the potential use case of improving on-chain trading algos. This non-linearity measure can be used by smart contracts to detect momentum or mean-reversion and improve existing signals or do smarter execution based on LP conditions.

We leverage [Shakudo Platform](https://shakudo.io/) which makes it easy to spin up RPC and Proof-of-Stake validator nodes (ex. Weavechain, BTC, ETH, Solana, Polygon and others) and run large scale data analytics on any dataset. On top of it [Weavechain](https://www.weavechain.com/)'s features make it easy to push data on-chain using Chainlink while offering lineage guarantees for the off-chain processing.

We want to enable all of the complex logic that happens in Web2 trading to be available in Web3 trading. 
There are two things just starting in Web3 trading that are really exciting right now:

* The emergence of synthetic assets that are pegged to the value of real world assets like AAPL stock
* Native Web3 trading arbitrage that takes advantage of things like multi-leg trades executing in the same block and flash loans. 

We want to make data available on-chain for Web3 consumers as easily as possible, with no restraints.

Demo video: https://www.youtube.com/watch?v=EWC6LGWjYQk