import logging
import time
import socket
import json
import traceback

from flask import Flask, jsonify, request

from weaveapi import weaveapi
from weaveapi.records import *
from weaveapi.options import *

WEAVE_CONFIG = "weave.config"


with open(WEAVE_CONFIG) as f:
    config = json.loads(f.read())
    print(config)

organization = config["organization"]
account = config["account"]
scope = config["scope"]
table = config["table"]


chainConfig = config["chainClientConfig"]
nodeApi = weaveapi.create(chainConfig)
nodeApi.init()

print(nodeApi.ping().get())

session = nodeApi.login(organization, account, scope).get()

app = Flask(__name__)

route = config["route"]

@app.route(route)
def flask_main():
    try:
        reply = nodeApi.read(session, scope, table, None, READ_DEFAULT_NO_CHAIN).get()
        output = { 'now': time.time() }
        for it in reply["data"]:
            data = json.loads(it["data"])
            output[data["pair"]] = {
                "ts": it["id"],
                "h": data["h"]
            }

        return jsonify(output)
    except:
        print(traceback.format_exc())
        return jsonify({
            'now': time.time(),
            'error': "Failed data retrieval"
        })


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=9999, threaded=True)
